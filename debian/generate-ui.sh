#!/bin/bash

set -e

SRCDIR=/usr/share/gocode/src/github.com/prometheus/alertmanager/ui/app
DSTDIR=/usr/share/prometheus/alertmanager/ui

ASROOT=sudo

echo "Installing dependencies..." >&2
$ASROOT apt install --no-install-recommends elm-compiler libjs-bootstrap4 \
    fonts-font-awesome curl uglifyjs golang-github-prometheus-alertmanager-dev

TMPDIR=$(mktemp -d)
trap "rm -rf $TMPDIR" EXIT

echo "Compiling source code..." >&2
ln -s $SRCDIR/src $SRCDIR/elm.json $TMPDIR
(cd $TMPDIR; elm make src/Main.elm --optimize --output $TMPDIR/app.js)

echo "Optimising source code..." >&2
uglifyjs $TMPDIR/app.js \
    --compress 'pure_funcs="F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9",pure_getters,keep_fargs=false,unsafe_comps,unsafe' \
    --mangle --output $TMPDIR/script.js

echo "Installing in Alertmanager directory..." >&2
$ASROOT mkdir -p $DSTDIR $DSTDIR/lib
$ASROOT cp $TMPDIR/script.js $SRCDIR/index.html $SRCDIR/favicon.ico $DSTDIR
test -L $DSTDIR/lib/elm-datepicker || \
    $ASROOT ln -s -T $SRCDIR/lib/elm-datepicker $DSTDIR/lib/elm-datepicker
test -L $DSTDIR/lib/font-awesome || \
    $ASROOT ln -s -T /usr/share/fonts-font-awesome $DSTDIR/lib/font-awesome
test -L $DSTDIR/lib/bootstrap || \
    $ASROOT ln -s -T /usr/share/nodejs/bootstrap/dist $DSTDIR/lib/bootstrap

echo "Finished! Please, restart prometheus-alertmanager to activate UI." >&2
